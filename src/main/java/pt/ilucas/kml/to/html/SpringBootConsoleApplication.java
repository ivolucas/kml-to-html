package pt.ilucas.kml.to.html;

import com.sun.istack.NotNull;
import de.micromata.opengis.kml.v_2_2_0.Data;
import de.micromata.opengis.kml.v_2_2_0.Document;
import de.micromata.opengis.kml.v_2_2_0.Feature;
import de.micromata.opengis.kml.v_2_2_0.Folder;
import de.micromata.opengis.kml.v_2_2_0.Kml;
import de.micromata.opengis.kml.v_2_2_0.Placemark;
import de.micromata.opengis.kml.v_2_2_0.Point;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.util.StringUtils;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import pt.ilucas.kml.to.html.dto.ExtraInfo;
import pt.ilucas.kml.to.html.dto.PointOfInterest;
import pt.ilucas.kml.to.html.dto.Section;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ilucas
 */
@SpringBootApplication
public class SpringBootConsoleApplication
        implements CommandLineRunner {

    private static Logger LOG = LoggerFactory
            .getLogger(SpringBootConsoleApplication.class);

    public static void main(String[] args) {
        LOG.info("STARTING THE APPLICATION");
        SpringApplication.run(SpringBootConsoleApplication.class, args);
        LOG.info("APPLICATION FINISHED");
    }

    IconIndex iconIndex = new IconIndex();

    @Autowired
    TemplateEngine templateEngine;

    @Override
    public void run(String... args) {
        LOG.info("EXECUTING : command line runner");

        for (int i = 0; i < args.length; ++i) {
            LOG.info("args[{}]: {}", i, args[i]);

        }

        File dir = new File("out");
        if (!dir.exists()) {
            dir.mkdirs();
        }
        try {
            File file = new File("Suica_latest.kmz");
            extractContent(file, dir);

            Kml kml = Kml.unmarshal(new File(dir, "doc.kml"));
            Document document = (Document) kml.getFeature();
            iconIndex.load(document.getStyleSelector());
            List<Section> sections = new ArrayList<>();
            for (Feature feature : document.getFeature()) {
                if (feature instanceof Folder) {
                    sections.add(parce((Folder) feature));
                }
            }

            build(sections, new File(dir, "out.html"));
        } catch (IOException ex) {
            LOG.error("Error", ex);
        }

    }

    public void extractContent(
            @NotNull File file, File dir)
            throws IOException {
        Kml[] EMPTY_KML_ARRAY = (new Kml[0]);
        if (!file.getName().endsWith(".kmz")) {
            return;
        }
        ZipFile zip = new ZipFile(file);
        Enumeration<? extends ZipEntry> entries = zip.entries();
        if (!file.exists()) {
            return;
        }
        while (entries.hasMoreElements()) {
            ZipEntry entry = ((ZipEntry) entries.nextElement());
            if (entry.getName().contains("__MACOSX") || entry.getName().contains(".DS_STORE")) {
                continue;
            }
            String entryName = URLDecoder.decode(entry.getName(), "UTF-8");
            InputStream in = zip.getInputStream(entry);
            File f = new File(dir, entryName);
            f.getParentFile().mkdirs();
            Files.copy(in, f.toPath(),StandardCopyOption.REPLACE_EXISTING);
        }
        zip.close();

    }

    public void build(List<Section> sections, File file) throws IOException {
        Context context = new Context();
        context.setVariable("sections", sections);
        try (FileWriter fw = new FileWriter(file)) {
            templateEngine.process("templatev1", context, fw);
        }
    }

    public Section parce(Folder folder) {
        System.out.println("Folder:" + folder.getName());
        Section section = new Section();
        section.setTitle(folder.getName());
        for (Feature feature : folder.getFeature()) {
            if (feature instanceof Placemark) {
                section.getPois().add(parce((Placemark) feature));
            }

        }
        return section;
    }

    public PointOfInterest parce(Placemark placemark) {
        PointOfInterest pointOfInterest = new PointOfInterest();
        pointOfInterest.setName(placemark.getName());
        pointOfInterest.setDescription(placemark.getDescription());
        if (placemark.getGeometry() instanceof Point) {
            pointOfInterest.setCoordinates(((Point) placemark.getGeometry()).getCoordinates().get(0));
        }
        pointOfInterest.setIconUrl(iconIndex.getIconFromStyleUrl(placemark.getStyleUrl()));
        if (placemark.getExtendedData() != null) {
            for (Data data : placemark.getExtendedData().getData()) {
                if (data.getName().equalsIgnoreCase("description") || data.getName().equalsIgnoreCase("descrição")) {
                    pointOfInterest.setDescription(data.getValue().replaceAll("<img.*>", "").replaceAll("\n", "<br/>"));
                } else if (data.getName().equalsIgnoreCase("gx_media_links")) {
                    pointOfInterest.setImageUrl(data.getValue().split(" "));
                } else {
                    if (StringUtils.hasText(data.getValue())) {
                        pointOfInterest.getExtraInfos().add(new ExtraInfo(data.getName(), data.getValue()));
                    }
                }

            }
        }
        return pointOfInterest;
    }
}
