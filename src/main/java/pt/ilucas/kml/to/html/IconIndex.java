/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ilucas.kml.to.html;

import de.micromata.opengis.kml.v_2_2_0.Pair;
import de.micromata.opengis.kml.v_2_2_0.Style;
import de.micromata.opengis.kml.v_2_2_0.StyleMap;
import de.micromata.opengis.kml.v_2_2_0.StyleSelector;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author ilucas
 */
public class IconIndex {

    Map<String, String> styleMap = new HashMap<>();
    Map<String, String> styleIcon = new HashMap<>();

    public void load(List<StyleSelector> list) {
        for (StyleSelector styleSelector : list) {

            if (styleSelector instanceof Style) {
                Style s = ((Style) styleSelector);
                if (s.getIconStyle() != null && s.getIconStyle().getIcon() != null) {
                    String href = ((Style) styleSelector).getIconStyle().getIcon().getHref();
                    styleIcon.put("#" + styleSelector.getId(), href);
                }

            } else if (styleSelector instanceof StyleMap) {
                List<Pair> pairList = ((StyleMap) styleSelector).getPair();
                if (pairList != null && !pairList.isEmpty()) {
                    styleMap.put("#" + styleSelector.getId(), pairList.get(0).getStyleUrl()); // exclude first char #
                }
            }
        }
    }

    public String getIconFromStyleUrl(String styleId) {
        String style = styleMap.get(styleId);
        if (style != null) {
            return styleIcon.get(style);
        } else {
            return styleIcon.get(styleId);
        }
        
    }
}
