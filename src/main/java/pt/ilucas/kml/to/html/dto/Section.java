/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ilucas.kml.to.html.dto;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ilucas
 */
public class Section {
    String title;
    List<PointOfInterest> pois = new ArrayList<>();

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<PointOfInterest> getPois() {
        return pois;
    }

    public void setPois(List<PointOfInterest> pois) {
        this.pois = pois;
    }
    
    
}
