/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ilucas.kml.to.html.dto;

/**
 *
 * @author ilucas
 */
public class ExtraInfo {
    String title;
    String description;

    public ExtraInfo(String title, String description) {
        this.title = title;
        this.description = description;
    }

    
    
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    
}
